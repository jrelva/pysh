#ipySh: UNIX Command line tools in Python

## About

ipySh allows you to use UNIX tools in a POSIX like way, but with Python!  

Designed to be used as a shell like tool in interperators like iPython.

At a basic level it is a wrapper for subprocess.Popen() with a custom class extension of the __builtin__.str object.

pySh can be used in a number of ways. In the most common use case we create a new pySh instance with some input text and then run one or more UNIX command line tools in a chain.

    In  [1]: print pySh('some\ninput\ntext').run('grep', 'input')
    Out [1]: 'input'

## Using With Interperators
pySh is primarily designed to be used inside a inteperator environment with either the built-in Python or an enhanced version such as iPython.

    In  [1]: from ipysh import pySh
    In  [2]: sh = pySh()

You can then use the sh instance like a UNIX shell:

    In  [3]: sh.df('-h').cowsay()

## Running Commands

There are a few ways to run a command in pySh. The preffered way (read: most explicit) is by calling the pySh.run() function:

    pySh().run('ping', '-c', '1', 'google.com')

The first parameter to the pySh.run() function is the command. Every extra pamater is passed to the command.

You can also use the name of the command as a function instead of run() for shorthand. pySh will dynamically create a lambda wrapper to run() during runtime:

    pySh().ping('-c','1','google.com')

This is assigned dynamically via a the pySh.\_\_getattr\_\_() function.

You may also specify a full path to the command being run, you cannot use the shorthand method for explicit paths:

    pySh().run('/usr/bin/ping', '-c', '1', 'google.com')

## Finding full path of a command
pySh.run() will call pySh.is_executable() for the given command name in order to locate the full path of the command in the system. pySh.is_executable() will search the system in the following order of preference:

  * The command as an explicit full path
  * In the current working directory
  * Searching the PATH environment

If the full path is not found an AttributeError will be raised.

## Chaining Commands
Chaining commands is a key feature of pySh, use it in much the same way you would pipes in a UNIX shell:

    In  [1]: print pySh('row1\tdata1\nrow2\tdata2\nrow3\tdata3').grep('row2').awk('{print $2}')
    Out [1]: data2

Although note this isn't a full implementation of the UNIX pipeline.

## Command Output
Every command returns a new pySh instance with the stdout and stderr of the command injected into the new instance.
Functionally, it works like the following pseudocode:

    class pySh(stdin):
      def run(command):
        stdout = run_command(command, stdin)
        return pySh(stdout)

However a few extra variables are assigned to the new instance:

    pySh.stdout
    pySh.stderr
    pySh.returncode

All of these variables are type(None) by default.

Example:

    In  [1]: shell = pySh('some\ninput\ntext')
    In  [2]: grep = shell.run('grep', 'input')
    In  [3]: print grep.stdout
    Out [1]: 'input'
    In  [4]: print grep.returncode
    Out [2]: '0'
    In  [5]: print grep.stderr
    Out [3]: None

As you can see the grep command did not output anything to stderr so grep.stderr remains type(None).

### Using pySh as a string
A pySh instance is an extension of the \_\_builtin\_\_.str class so you can use command outputs just like you could any other string:

    In  [1]: pySh().echo('foo').upper()
    Out [1]: 'FOO'

Note that this will return a type(str) object. You can convieniently use pySh.cast() to convert each object in a list or set to pySh instances:

    In  [1]: output = pySh('1\n1\n1').splitlines()
    Out [1]: ['1', '1', '1']
    In  [2]: type(output[0])
    Out [2]: str
    In  [3]: output = pySh().cast(output)
    In  [4]: print type(output[0])
    Out [3]: pySh

You could also simply create a new pySh instance with a string object:

    In  [1]: foo = pySh().echo('foo').upper()
    In  [2]: type(foo)
    Out [1]: str 
    In  [3]: foo = pySh(foo)
    In  [4]: print type(foo)
    Out [2]: pySh

### Merging stdout and stderr
You can redirect stderr to stdout by setting the pySh.merge_output variable prior to execution of pySh.run() on that instance:

    In  [1]: sh = pySh()
    In  [2]: sh.merge_output = True
    In  [3]: ping = sh.run('ping', 'notarealhost')
    In  [4]: print ping.stdout
    Out [1]: 'ping: unknown host notarealhost'

In this mode pySh.stderr is always None:

    In  [5]: print ping.stderr
    Out [2]: None

The pySh.merge_output variable is passed to all children of the instance.

Note that you must set this variable before calling pySh.run() as the output redirection happens on the OS level during execution. 

It is not possible to change this variable when using a chain of commands, although one could break the chain apart into separate variables and set pySh.merge_output for those.

## KeyboardInterrupt
If a KeyboardInterrupt is raised during execution of a command then the command is terminated via subprocess.Popen.terminate() and stdout and stderr set to an empty pySh instance.

In future it will return the stdout and stderr up to the point of termination.



