__author__ = 'jkingsbury'

import unittest

from ipysh import pySh


class pyShTestCase(unittest.TestCase):
    def setUp(self):
        self.shell = pySh('one\ntwo\nthree')

class pyShMergedOutputTestCase(unittest.TestCase):
    def setUp(self):
        self.shell = pySh('one\ntwo\nthree')
        self.shell.merge_output = True


class missingpathTestCase(pyShTestCase):
    def runTest(self):
        self.assertRaises(AttributeError, self.shell.run, 'notarealprogram')


class basicrunTestCase(pyShTestCase):
    def runTest(self):
        self.assertEqual(self.shell.run('wc', '-l'), '2')


class stdoutTestCase(pyShTestCase):
    def runTest(self):
        self.assertNotEqual(self.shell.run('grep', '-v', 'three').stdout, '')
        self.assertNotEqual(self.shell.run('grep', '-v', 'three').stdout, None)


class stderrTestCase(pyShTestCase):
    def runTest(self):
        self.assertNotEqual(self.shell.run('grep', '-notaswitch').stderr, '')
        self.assertNotEqual(self.shell.run('grep', '-notaswitch').stderr, None)


class mergedOutputTestCase(pyShMergedOutputTestCase):
    def runTest(self):
        grep_cmd = self.shell.run('grep', '-notaswitch')
        self.assertNotEqual(grep_cmd.stdout, None)
        self.assertEqual(grep_cmd.stderr, None)


class returncodeTestCase(pyShTestCase):
    def runTest(self):
        self.assertNotEqual(self.shell.run('grep', '-notaswitch').returncode, 0)

class arbitrarycommandTestCase(pyShTestCase):
    def runTest(self):
        self.assertEqual(self.shell.echo('TEST'), 'TEST')

if __name__ == "__main__":
    unittest.main()
